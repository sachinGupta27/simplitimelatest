





import React, { Component } from 'react';
import {
    StyleSheet,
    Text, TouchableWithoutFeedback,
    View, Image, TouchableOpacity,
} from 'react-native';


const FloatingButton = ({ onButtonPressed, icon }) => {
    return (
        <TouchableWithoutFeedback onPress={onButtonPressed}>
            <View activeOpacity={0.5}
                style={{ position: "absolute", right: 10, bottom: 5, zIndex: 99, }}>
                <Image source={icon} />
            </View>
        </TouchableWithoutFeedback>
    )
}






export default FloatingButton;