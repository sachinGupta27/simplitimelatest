




/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, Image
} from 'react-native';
import { height, width, fontSizes, getLog } from '../utils/utils'

// const graphData=[
//     {}
// ]
var graphData = [
  { day: "Mon", time: 8 }, { day: "Tue", time: 10 }, { day: "Wed", time: 9 }, { day: "Thu", time: 11 }
  , { day: "Fri", time: 11 }, { day: "Sat", time: 3 }, { day: "Sun", time: 0 }
]
export default class activityList extends Component {
  constructor(props) {

    super(props)
    this.state = {
      timeArr: [],
      graphDataPercentageArray: [],
      //graphData:graphData
    }
  }
  componentWillMount() {
    // setTimeout(() => {
    //   alert(JSON.stringify(this.props.dataArr))
    // }, 1000)
    if (this.props.dataArr) {

      // var graphData = [
      //   { day: "Mon", time: 8 }, { day: "Tue", time: 10 }, { day: "Wed", time: 9 }, { day: "Thu", time: 11 }
      //   , { day: "Fri", time: 11 }, { day: "Sat", time: 3 }, { day: "Sun", time: 0 }
      // ]
      var graphData = this.props.dataArr
      for (var i = 0; i < graphData.length; i++) {
        graphData[i].time = (graphData[i].time) / 60
      }
      var max = '';
      for (var i = 0; i < graphData.length; i++) {
        if (!max || parseInt(graphData[i].time) > parseInt(max.time))
          max = graphData[i];
        //getLog("max" + JSON.stringify(max))
      }
      max = max.time % 2 == 0 ? max.time : max.time + 1
      //getLog(graphData.length)
      var arr = []
      for (var i = 0; i <= max; i = i + 2) {
        arr.push(i)
      }
      var graphDataPercentageArray = []
      for (var i = 0; i < graphData.length; i++) {
        var data = graphData[i]
        data.time = "" + ((data.time * 100) / max) + "%"

        graphDataPercentageArray.push(data)
      }

      this.setState({
        timeArr: arr,
        graphDataPercentageArray: graphDataPercentageArray
      })

    }


  }
  lineGraph(percentage, i) {
    return (
      <View key={"view" + i} style={{ flex: 1, height: percentage == '0%' ? '2%' : percentage, backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
    )
  }
  returnGraph() {
    var lineGraph = []
    for (var i = 0; i < 7; i++) {
      lineGraph.push(
        this.lineGraph(this.state.graphDataPercentageArray[i].time, i)
      )
    }
    return (
      lineGraph.reverse()
    )
  }
  timeGraph(time, i) {
    return (
      <Text key={"text" + i} style={{ margin: 2, fontSize: 12 }}>{time}</Text>
    )
  }
  returnTime() {
    var time = []
    for (var i = 0; i < this.state.timeArr.length; i++) {
      time.push(
        this.timeGraph(this.state.timeArr[i], i)
      )
    }
    return (
      time.reverse()
    )
  }
  dayGraph(day, i) {
    return (
      <Text key={"day" + i} style={{ flex: 1, marginHorizontal: 5 }}>
        {day}
      </Text>
    )
  }
  returnDays() {
    var days = []
    days.push(
      this.dayGraph('', -1)
    )
    for (var i = 0; i < 7; i++) {
      days.push(
        this.dayGraph(this.state.graphDataPercentageArray[i].day, i)
      )
    }
    return (
      days
    )
  }
  render() {

    return (
      <View>
        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 15, transform: [{ rotate: '180deg' }] }}>
          {this.returnGraph()}

          <View style={{ flex: 1, justifyContent: "space-between", height: width / 1.5, marginHorizontal: 5, transform: [{ rotate: '180deg' }], marginRight: 20, marginLeft: 0 }}>
            {this.returnTime()}

          </View>

        </View>

        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginLeft: 15 }}>

          {this.returnDays()}

        </View>
      </View>
    );
  }
}







