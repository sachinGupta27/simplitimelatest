/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, Image
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'
import Setting from './setting'


export default class activityList extends Component {
    static navigationOptions = {
        // drawerLabel: global.userType=='Admin' ? 'sdfsdfsd': 'Activity List',
        drawerLabel: () => global.userType == 'Admin' ? null : 'Activity List',
        
        drawerIcon: ({ tintColor }) => global.userType == 'Admin' ? null : (
            <Image style={{ tintColor: tintColor }}
                source={require('../images/drawerIcons/activityListIcon.png')}
            />
        ),
    };
    render() {
        return (
            <Setting id="activityList" navigation={this.props.navigation} />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    welcome: {
        fontSize: fontSizes.font20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
