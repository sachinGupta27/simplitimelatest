/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  Alert,
  StyleSheet,
  TextInput,
  ScrollView,
  Text,
  Keyboard,
  AsyncStorage,
  Linking,
  View,
  Image,
  SafeAreaView,
  TouchableWithoutFeedback
} from "react-native";
//import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { StackActions, NavigationActions } from "react-navigation";
import { height, width, fontSizes, getLog } from "../utils/utils";
import {
  dateConverter,
  week_Date_Array,
  year_Array,
  dateConverterUseInWeekTab,
  dateConverterUseInHeader,
  hourMinuteConverter
} from "../components/dateConverter";
import Header from "../components/commonHeader";

import webservice from "../components/webService";
import Spinner from "../components/spinner";

export default class activityList extends Component {
  static navigationOptions = {
    drawerLabel: "Company Profile",
    drawerIcon: ({ tintColor }) => (
      <Image
        style={{ tintColor: tintColor }}
        source={require("../images/drawerIcons/compnyIcon.png")}
      />
    )
  };
  constructor(props) {
    super(props);
    //alert(JSON.stringify(this.props.navigation.state.params.props.navigation.openDrawer()))
    // alert(JSON.stringify(this.props.navigation.state.params.props.state))
    this.state = {
      userImage: require("../images/userImage.png"),
      //userImage: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLb5mOUtzV0ObqBVuAURSvPAsC27148aFdKGc6e6Z_Z78vmMWf' },
      image64: "",
      name: "",
      address: "",
      nameEdit: false,
      addressEdit: false,
      spinnerVisible: false,
      userToken: "",
      userId: "",
      companyId: ""
    };
  }
  componentDidMount() {
    AsyncStorage.getItem("userToken").then(token => {
      this.setState({ userToken: token });
    });
  }
  componentWillMount() {
    AsyncStorage.getItem("LoginresponseData").then(response => {
      var data = JSON.parse(response);
      //this.setState({ email: data.email })
      this.setState({
        name: data.companyName,
        companyId: data.companyCode,
        address: data.address ? data.address : ""
      });
      //getLog(data.thumbPhoto)
    });
  }

  onChange(text, type) {
    if (type === "Name") {
      this.setState({ name: text });
    }
    if (type === "Address") {
      this.setState({ address: text });
    }
  }
  hitCompanyProfileAPI(hitFrom, imageBase64) {
    let variables = new FormData();

    variables.append("companyName", this.state.name);
    variables.append("address", this.state.address);

    this.setState({ spinnerVisible: true });
    return webservice(
      variables,
      "user/editcompany",
      "POST",
      this.state.userToken
    ).then(response => {
      this.setState({ spinnerVisible: false });

      if (response != "error") {
        this.setState({ addressEdit: false });
        this.setState({ nameEdit: false });

        setTimeout(() => {
          AsyncStorage.getItem("LoginresponseData").then(res => {
            res = JSON.parse(res);
            res.companyName = this.state.name;
            res.address = this.state.address;
            // res.thumbPhoto = { uri: response.userData.thumbPhoto }
            // this.setState({
            //     userImage: { uri: response.userData.thumbPhoto }
            // })
            AsyncStorage.setItem('LoginresponseData', JSON.stringify(res), () => {
                //Callback("callback called name changed" + this.state.name)
            });
          });
          //this.props.navigation.state.params.employeeDetailCallBack.callBack("data")
          //this.setState({ addEmployeeModalVisible: false })
        }, 300);
      } else {
      }
      getLog("response of edit user detail " + response);
    });
  }
  Callback;
  static companyProfileCallback(callback) {
    Callback = callback;
  }
  nameEdit() {
    if (this.state.address == "") {
      alert("Please update address first.");
    } else {
      if (this.state.nameEdit) {
        if (this.state.name != "") {
          this.hitCompanyProfileAPI("name");
        } else {
          alert("Please enter name.");
        }
      } else {
        if (this.state.addressEdit) {
          alert("Please save address first.");
        } else {
          this.setState({ nameEdit: true });
        }
      }
    }
  }
  addressEdit() {
      
    //var emailRegex = (/^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i)

    if (this.state.addressEdit) {
      if (this.state.address == "") {
        alert("Please enter address.");
      } else {
        this.hitCompanyProfileAPI("address");
      }
    } else {
      if (this.state.nameEdit) {
        alert("Please save company name first.");
      } else {
        this.setState({ addressEdit: true });
      }
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <Spinner visible={this.state.spinnerVisible} />
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.container}>
            <Header
              HeaderLeftText="   Company Profile"
              //HeaderRightText={this.state.datePassedToHeader != '' ? this.state.datePassedToHeader : today_To_day_AfterEighth_Day()}
              //type="date"
              //this.props.navigation.state.params
              //alert(JSON.stringify(this.props.navigation.state.params.props.navigation.state.openDrawer()))
              //toggleDrawer={() => this.props.navigation.state.params.props.state.routes.navigate('Drawer')}
              toggleDrawer={() => this.props.navigation.openDrawer()}
              //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
              // headerRightIcon={require('../images/analysisExportIcons/dateIcon.png')}
              //headerRightIconOnPress={() => this.showStartDatePicker()}
            />

            <ScrollView
              keyboardShouldPersistTaps="always"
              style={styles.detailContainer}
            >
              <View style={styles.rowView}>
                <View
                  style={{
                    flex: 1.8,
                    justifyContent: "center",
                    marginLeft: 15
                  }}
                >
                  <Text style={styles.textInputText}>Company Name</Text>
                </View>
                <View style={styles.textInputView}>
                  <TextInput
                    style={[
                      styles.textInput,
                      { color: this.state.nameEdit ? "#000" : "#979797" }
                    ]}
                    placeholder="CompanyName"
                    placeholderTextColor="#979797"
                    underlineColorAndroid="transparent"
                    onChangeText={text => this.onChange(text, "Name")}
                    value={this.state.name}
                    editable={this.state.nameEdit ? true : false}
                    autoFocus={this.state.nameEdit ? true : false}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>
                <View style={styles.editView}>
                  <TouchableWithoutFeedback onPress={() => this.nameEdit()}>
                    <Image
                      source={
                        this.state.nameEdit
                          ? require("../images/appSettingIcons/checkIconGrey.png")
                          : require("../images/appSettingIcons/editIconGrey.png")
                      }
                      resizeMode="cover"
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>

              <View style={styles.rowView}>
                <View
                  style={{ flex: 1, justifyContent: "center", marginLeft: 15 }}
                >
                  <Text style={styles.textInputText}>Address</Text>
                </View>
                <View style={styles.textInputView}>
                  <TextInput
                    style={[
                      styles.textInput,
                      { color: this.state.addressEdit ? "#000" : "#979797" }
                    ]}
                    placeholder="Address"
                    placeholderTextColor="#979797"
                    underlineColorAndroid="transparent"
                    onChangeText={text => this.onChange(text, "Address")}
                    value={this.state.address}
                    editable={this.state.addressEdit ? true : false}
                    autoFocus={this.state.addressEdit ? true : false}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                </View>
                <View style={styles.editView}>
                  <TouchableWithoutFeedback onPress={() => this.addressEdit()}>
                    <Image
                      source={
                        this.state.addressEdit
                          ? require("../images/appSettingIcons/checkIconGrey.png")
                          : require("../images/appSettingIcons/editIconGrey.png")
                      }
                      resizeMode="cover"
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>

              <TouchableWithoutFeedback
              >
                <View
                  style={[styles.rowView, { justifyContent: "space-between" }]}
                >
                  <View style={{ justifyContent: "center", marginLeft: 15 }}>
                    <Text style={styles.textInputText}>Company Id</Text>
                  </View>
                  <View style={{ justifyContent: "center", marginRight: 15 }}>
                    <Text style={[styles.textInputText, { color: "#979797" }]}>
                      {this.state.companyId}
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
              >
                <View
                  style={[styles.rowView, { justifyContent: "space-between" }]}
                >
                  <View style={{ justifyContent: "center", marginLeft: 15 }}>
                    <Text style={styles.textInputText}>8 Employees</Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </ScrollView>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: "#21459E"
  },
  container: { flex: 1, backgroundColor: "#fff" },
  imageRelativeView: { width: width, height: 50, backgroundColor: "#2A56C6" },
  imageViewExternal: {
    width: width / 3,
    height: width / 3,
    borderRadius: width / 3 / 2,
    backgroundColor: "white",
    alignSelf: "center",
    marginTop: -60,
    borderColor: "#fff",
    borderWidth: 1
  },
  imageViewInternal: {
    width: width / 3 - 8,
    height: width / 3 - 8,
    borderRadius: (width / 3 - 8) / 2,
    backgroundColor: "#21459E",
    alignSelf: "center",
    borderColor: "#21459E",
    marginTop: 2,
    borderWidth: 2
  },
  userImage: {
    height: width / 3 - 12,
    width: width / 3 - 12,
    alignSelf: "center",
    borderRadius: (width / 3 - 12) / 2
  },
  plusView: {
    height: 30,
    width: 30,
    borderRadius: 15,
    alignSelf: "flex-end",
    marginTop: Platform.OS == "ios" ? -25 : -30
  },
  detailContainer: { width: width, marginTop: 10 },
  rowView: {
    height: 50,
    width: width,
    flexDirection: "row",
    borderBottomColor: "#E1E2E4",
    borderBottomWidth: 1
  },
  textInputText: { fontSize: fontSizes.font14, color: "#000" },
  textInputView: { flex: 3, marginHorizontal: 10 },
  textInput: { height: 50, textAlign: "right" },
  editView: {
    flex: 0.5,
    marginRight: 15,
    alignItems: "flex-end",
    justifyContent: "center"
  },
  rightArrowView: {
    marginRight: 15,
    alignItems: "flex-end",
    justifyContent: "center"
  }
});
