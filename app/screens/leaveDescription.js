



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, AppState,
    Text, ScrollView, TouchableWithoutFeedback,
    View, Image, SafeAreaView, Keyboard, AsyncStorage
} from 'react-native';
import Header from '../components/commonHeader'
import { height, width, fontSizes, getLog } from '../utils/utils'
import { dateConverterUseInWeekTab, dateConverterUseInHeader } from '../components/dateConverter'
import LeaveModal from '../components/leaveModal'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
var keyboardOpen = false
export default class leaveDescription extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            appState: AppState.currentState,
            modalVisible: false,
            datePickerVisible: false,
            startDateToShowOnMOdal: '',
            endDateToShowOnModal: '',
            totalDays: 0,
            minDate: '',
            reasonTextInputData: '',
            leaveTypeOnLeaveModal: 'Leave Type',
            startDate: '',
            endDate: '',
            leaveTypeList: [],
            leaveTypeListPassToModal:[],
            index: this.props.navigation.state.params.index,
            errorText: '', status: '',
            userToken: '',
            spinnerVisible:false

        }
    }
    componentWillMount() {
        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })

        })
        // alert(JSON.stringify(this.props.navigation.state.params.leaveTabCallBack))
        //alert(JSON.stringify(this.props.navigation.state.params.data))
        this.setState({ data: this.props.navigation.state.params.data })
        this.setState({
            startDateToShowOnMOdal: dateConverterUseInHeader(new Date(this.props.navigation.state.params.data.validityFrom)),
            endDateToShowOnModal: dateConverterUseInHeader(new Date(this.props.navigation.state.params.data.validityTo)),
            totalDays: this.props.navigation.state.params.data.totalDays,
            reasonTextInputData: this.props.navigation.state.params.data.reason,
            leaveTypeOnLeaveModal: this.props.navigation.state.params.data.type,
            startDate: this.props.navigation.state.params.data.validityFrom,
            endDate: this.props.navigation.state.params.data.validityTo,
            minDate: this.props.navigation.state.params.data.validityFrom,
            leaveId: this.props.navigation.state.params.data.leaveId,
            leaveTypeId: this.props.navigation.state.params.data.leaveTypeId,
            status: this.props.navigation.state.params.data.status,
            leaveTypeListPassToModal: this.props.navigation.state.params.leaveTypeListPassToModal,
            leaveTypeList:this.props.navigation.state.params.leaveTypeList,

        })
        // setTimeout(() => {
        //     alert(JSON.stringify(this.state.data))
        // }, 500)
    }
    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
    }
    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    keyboardOpen() {
        keyboardOpen = true
        //this.setState({ keyboardOpen: true })
    }

    keyboardClose() {
        keyboardOpen = false
        //this.setState({ keyboardOpen: false })
    }
    startDateVisibleFunction() {
        this.setState({ datePickerVisible: true, datePickerType: 'start', minDate: '' })
    }
    endDateVisibleFunction() {
        if (this.state.startDate == '') {
            alert("Please select start date first")
        } else {
            this.setState({ datePickerVisible: true, datePickerType: 'end', minDate: this.state.minDate })
        }

    }
    getDateFromModal(date) {
        getLog("getDateFromModal" + date)
        if (this.state.datePickerType == 'start') {
            this.setState({ startDate: date, datePickerType: '', minDate: date, datePickerVisible: false, endDateToShowOnModal: "", endDate: '' })
            var d = dateConverterUseInHeader(date)
            setTimeout(() => {
                this.setState({ startDateToShowOnMOdal: d })
            }, 200)
        } if (this.state.datePickerType == 'end') {
            this.setState({ endDate: date, datePickerType: '', datePickerVisible: false, })
            var d = dateConverterUseInHeader(date)
            setTimeout(() => {
                this.setState({ endDateToShowOnModal: d })
                this.totalDaysCalculate()
            }, 200)

        }


    }
    hitEditLeaveAPI() {
        var arr = this.state.leaveList
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("leaveTypeId", this.state.leaveTypeId)
        variables.append("days", this.state.totalDays)
        variables.append("startDate", new Date(this.state.startDate).getTime())
        variables.append("endDate", new Date(this.state.endDate).getTime())
        variables.append("reason", this.state.reasonTextInputData)
        variables.append("leaveId", this.state.leaveId)

        this.setState({ spinnerVisible: true })
        return webservice(variables, "leave/edit", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {

                    this.setState({
                        startDateToShowOnMOdal: dateConverterUseInHeader(new Date(this.state.startDate)),
                        endDateToShowOnModal: dateConverterUseInHeader(new Date(this.state.endDate)),
                        totalDays: this.state.totalDays,
                        reasonTextInputData: this.state.reasonTextInputData,
                        leaveTypeOnLeaveModal: this.state.leaveTypeOnLeaveModal,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        minDate: this.state.startDate,
                        leaveId: this.state.leaveTypeId,
                        leaveTypeId: this.state.leaveTypeId,
                        status: this.state.status,


                    })


                    var data = {
                        "validityFrom": this.state.startDate,
                        "validityTo": this.state.endDate,
                        'leaveId': this.state.leaveId,

                        'leaveTypeId': this.state.leaveTypeId,
                        'type': this.state.leaveTypeOnLeaveModal,
                        "status": this.state.status,
                        'totalDays': this.state.totalDays,
                        "reason": this.state.reasonTextInputData
                    }



                    setTimeout(() => {
                        this.setState({ modalVisible: false })

                    }, 300)
                    //getLog("=====++++" + this.state.leaveTypeOnLeaveModal)
                    this.props.navigation.state.params.leaveTabCallBack.callBack({ type: "edit", index: this.state.index, editedData: data })

                } else {
                    getLog("Leave  edit response=====++++" + JSON.stringify(response))

                }

            })
    }
    totalDaysCalculate() {
        getLog(this.state.endDate)
        var date1 = new Date(this.state.startDate);
        var date2 = new Date(this.state.endDate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
        this.daysTextInputChangeOnLeaveModal("" + diffDays)
        // this.setState({ totalDays: diffDays + 1 })
        // setTimeout(() => {
        //   console.log(this.state.totalDays)
        // }, 300)



    }
    daysTextInputChangeOnLeaveModal(text) {
        this.setState({ totalDays: text })
    }
    saveButtonClicked() {
        this.hitEditLeaveAPI()
        // var data = {
        //     "validityFrom": this.state.startDate,
        //     "validityTo": this.state.endDate,
        //     'type': this.state.leaveTypeOnLeaveModal,
        //     "status": this.state.status,
        //     'totalDays': this.state.totalDays,
        //     "reason": this.state.reasonTextInputData
        // }
        // this.setState({
        //     data: data
        // })


        // setTimeout(() => {
        //     this.setState({ modalVisible: false })

        // }, 300)
        // getLog("=====++++" + this.state.leaveTypeOnLeaveModal)
        // this.props.navigation.state.params.leaveTabCallBack.callBack({ type: "edit", index: this.state.index, editedData: data })


        //alert(this.state.reasonTextInputData)
    }
    onLeaveModalselectLeaveTypeClicked(index, value) {
        
        this.setState({ leaveTypeOnLeaveModal: value, leaveTypeId:this.state.leaveTypeList[index].leaveTypeId })
    }
    modalClose() {
        if (keyboardOpen) {
            Keyboard.dismiss()
        } else {
            this.setState({ modalVisible: false })
        }

    }
    Delete() {

        this.props.navigation.state.params.leaveTabCallBack.callBack({ type: "delete", index: this.state.index })
        this.props.navigation.goBack()
    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Header
                        HeaderLeftText={dateConverterUseInWeekTab(new Date(this.state.data.validityFrom)) + "-" + dateConverterUseInWeekTab(new Date(this.state.data.validityTo)) + " | " + this.state.data.totalDays + (this.state.data.totalDays > 1 ? " Days" : " Day")}
                        headerType='back'
                        //HeaderRightText=''
                        type="leaveDescription"
                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={this.state.status == 0 ? require('../images/drawerIcons/editIcon.png') : null}
                        headerRightIconOnPress={() => this.state.status == 0 ? this.setState({ modalVisible: true }) : null}
                    />
<Spinner visible={this.state.spinnerVisible} />
                    <LeaveModal
                        modalVisible={this.state.modalVisible}
                        datePickerVisible={this.state.datePickerVisible}
                        datePickerVisibleFunction={() => this.setState({ datePickerVisible: true })}
                        startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                        endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                        dateSelected={(date) => this.getDateFromModal(date)}
                        startDate={this.state.startDateToShowOnMOdal}
                        endDate={this.state.endDateToShowOnModal}
                        daysTextInputData={this.state.totalDays}
                        daysTextInputChange={(text) => this.daysTextInputChangeOnLeaveModal(text)}
                        datePickerCloseFunction={() => this.setState({ datePickerVisible: false })}
                        minDate={this.state.minDate == '' ? new Date('01/01/1990') : new Date(this.state.minDate)}
                        saveButtonClicked={() => this.saveButtonClicked()}
                        reasonTextInputDataChange={(text) => this.setState({ reasonTextInputData: text })}
                        reasonTextInputData={this.state.reasonTextInputData}
                        modalClose={() => this.modalClose()}
                        TitleText="Add Leave" TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
                        leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassToModal}
                        selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
                        errorText={this.state.errorText}
                    />

                    <View style={styles.mainContainer}>

                        <View style={styles.rowView}>
                            <Text style={[styles.textStyle, { fontSize: fontSizes.font16 }]}>{this.state.data.type}</Text>
                            <Text style={[styles.textStyle, {
                                marginRight: 20,
                                color: this.state.status == 0 ? "red" : this.state.status == 1 ? "green" : "grey",
                                fontSize: fontSizes.font16
                            }]}>{this.state.status == 0 ? "Pending" : this.state.status == 1 ? "Approved" : "Rejected"}</Text>
                        </View>
                        <Text style={styles.textStyle}>Reason</Text>
                        <Text style={[styles.textStyle, { color: "#979797", marginTop: 0 }]}>{this.state.data.reason}</Text>


                    </View>
                    <TouchableWithoutFeedback onPress={() => this.Delete()}>
                        <View style={styles.buttonStyle} >
                            <Text style={[styles.titleText, {}]}>
                                Delete
                                </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#fff'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: { padding: 10, borderBottomColor: "#E5E5E5", borderBottomWidth: 1 },
    rowView: { flexDirection: "row", justifyContent: "space-between" },
    textStyle: { fontSize: fontSizes.font14, color: "#000", textAlign: 'left', margin: 10 },
    titleText: { marginLeft: 5, color: 'white', textAlign: 'center', fontSize: fontSizes.font16, marginTop: 0 },
    buttonStyle: { backgroundColor: "#2A56C6", height: 50, alignItems: "center", justifyContent: 'center', marginTop: 50, borderRadius: 3, margin: 15 }
});
