import React, { Component } from 'react';
import {
    Platform,PanResponder,
    StyleSheet, Picker, ScrollView,
    Text, SafeAreaView, TouchableOpacity,
    View, Image,
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'
import { dateConverter, week_Date_Array, year_Array, dateConverterUseInWeekTab, dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
import Header from '../components/commonHeader'

export default class activityList extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
        }

    }
    static navigationOptions = {
        drawerLabel: '',
       
    };
    componentDidMount(){
        
    }
    componentWillMount() {
        
    }
    
    
    render() {

        return (
            <SafeAreaView
                style={styles.safeArea}>
                <View style={styles.container}>
                    <Header HeaderLeftText="Edit Profile"
                        //HeaderRightText={this.state.datePassedToHeader != '' ? this.state.datePassedToHeader : today_To_day_AfterEighth_Day()}
                        //type="date"
                        headerType='back'
                         toggleDrawer={() => this.props.navigation.goBack()}
                        //toggleDrawer={() => this.props.navigation.openDrawer()}
                    //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                    // headerRightIcon={require('../images/analysisExportIcons/dateIcon.png')}
                    //headerRightIconOnPress={() => this.showStartDatePicker()} 
                    />
                    
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    container: { flex: 1, backgroundColor: '#fff', },
    tabStyle: { alignItems: "center", justifyContent: "center", width: width / 3, height: 35 },
    tabText: { fontSize: fontSizes.font12, color: "white", textAlign: 'center', },
});
