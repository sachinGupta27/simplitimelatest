
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, AppState,
    Text, SafeAreaView, Keyboard, TouchableWithoutFeedback,
    View, Image, ScrollView, TouchableOpacity, FlatList,
    DeviceEventEmitter, AsyncStorage, TextInput,
    NativeAppEventEmitter, PanResponder
} from 'react-native';




import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import LeaveModal from '../components/leaveModal'
import SelectProjectModal from '../components/selectProjectModal'
import PickerDropdownModal from '../components/pickerDropdownModal'

import TimeLeaveTab from '../components/timesheetLeaveTab'
import { height, width, fontSizes, getLog } from '../utils/utils'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
const monthArr = ["", "", "January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "", ""]
import { dateConverter, week_Date_Array, year_Array, dateConverterUseInWeekTab, dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
var interval
var keyboardOpen = false
//alert(JSON.stringify(week_Date_Array))
const data = [
    1, 2, 3, 4, 5, 6, 7, 8
]
var intervalId;
var backgroundSecond = 0
//getLog(JSON.stringify(week_Date_Array))
export default class timeSheet extends Component {
    constructor(props) {

        super(props);
        this.state = {
            week_Date_Array: week_Date_Array,
            globalLeft: '',
            appState: AppState.currentState,
            second: 0,
            minute: 0,
            TabPress: 'details',
            projectList: [],
            selectProjectModal: false,
            modalVisible: false,
            datePickerVisible: false, leaveType: 'Leave Type',
            leaveList: [],

            totalProjectHours: 0, totalLeaveDays: 0, totalDays: 0,
            returnCallBack: { value: false, callBack: () => { } },
            leaveTabCallBack: { value: false, callBack: () => { } },
            callbackPassedToProjectBreakdown: { value: false, callBack: () => { } },
            startDate: '', endDate: '', datePickerType: '', minDate: '', startDateToShowOnMOdal: '', endDateToShowOnModal: '',
            pickerDropdownModalVisible: false,
            selectProject: "Select Project",
            selectedProjectId: "",
            week: "Week",
            pickerDropdownModalTitleText: '',
            pickerDropdownModalListData: [],
            leaveTypeOnLeaveModal: 'Leave Type',
            leaveTypeList: [],
            leaveTypeListPassToModal: [],
            playPauseImage: '',
            play: false,
            playPauseText: '',
            stopImage: '',
            //week_Date_Array: week_Date_Array,
            week_Date_Array_Middle_Index: 24,
            weekArray: [],
            leaveModalErrorText: "", reasonTextInputData: '',
            spinnerVisible: false,
            userToken: '', projectListPassedToSelectProjectDropdown: [],
            projectListContainAllProjectOfSelectProjectDropdown: [],
            filterDataPassedOnProjectBreakdown: '',
            globalLeft: '',
            selectedProjectItem: '',



            arrayPassedToTabScrollView: week_Date_Array,


            //detail tab states
            name: '',
            email: '',
            userId: '',
            activeStatus: false,
            nameEdit: false,
            emailEdit: false,
            searchtextPassedbackToManagePage: ''

        }



    }


    // test for week scroll view






    //


    hitAllAPI() {
        this.setState({ spinnerVisible: true })
        setTimeout(() => {
            this.setState({ spinnerVisible: false })
        }, 7000)
        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })
            //this.hitGetAllLeaveTypesAPI(token)
            //this.hitGetLeaveListAPI(token,"week")
            //getLeaveListAPI is called in makeWeekDataTabArray
            //this.makeWeekDateTabArray(token)
            //this.hitGetAllProjectListApi(token)







        })

    }
    componentWillMount() {


        this.hitAllAPI()

        var days = 0
        var leaveTypeArr = []

        if (this.props.id) {
            this.props.id == 'leaves' ? this.setState({ TabPress: 'leaves' }) : null
        }
        // this.totalHoursMinutesOfProjects()

        //this.setState({ projectList: projectArr, })
        this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })
        this.setState({ leaveTabCallBack: { value: false, callBack: this.callBackCalledOnEditDeleteLeave } })
        this.setState({ callbackPassedToProjectBreakdown: { value: false, callBack: this.callbackCalledOnChangesOnProjectBreakdown } })
        this.setState({
            name: this.props.navigation.state.params.data.name,
            email: this.props.navigation.state.params.data.id,
            userId: this.props.navigation.state.params.data.userId,
            activeStatus: this.props.navigation.state.params.data.isActive == 'Disabled' ? false : true,
            searchtextPassedbackToManagePage: this.props.navigation.state.params.searchText
        })
        this.setState({ week_Date_Array: week_Date_Array })

        //this.updateSecondsOfTimerWhenControllerComesToThisScreen()

        this.wrapperPanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderEnd: (e, { vx, dx }) => {

                console.log("end" + dx)
                if (dx < 0) {
                    this.setState({ globalLeft: "right" })
                    if (this.state.week == 'Month') {
                        if (this.state.week_Date_Array_Middle_Index < 11) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)
                        }
                    } else {
                        if (this.state.week_Date_Array_Middle_Index < 49) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)
                        }
                    }

                    //alert("right")
                } else {
                    this.setState({ globalLeft: "left" })
                    if (this.state.week == 'Month') {



                        if (this.state.week_Date_Array_Middle_Index > 0) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)

                        }
                        //else {
                        //     //alert(this.state.week_Date_Array_Middle_Index)
                        //     if (this.state.week_Date_Array_Middle_Index > -1) {
                        //         this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                        //         this.scroller.scrollTo({ x: 0, y: 0 })
                        //         setTimeout(() => {
                        //             this.hitGetLeaveListAPI(this.state.userToken, "month", this.state.week_Date_Array_Middle_Index + 2)
                        //             this.hitGetProjectListAPI(this.state.userToken, "month", this.state.week_Date_Array_Middle_Index + 2)
                        //         })
                        //     }

                        // }
                    } else {
                        if (this.state.week_Date_Array_Middle_Index > 1) {
                            this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
                            setTimeout(() => {
                                this.scrollToPosition(this.state.userToken)
                            }, 300)
                        }
                    }
                    //alert("left")

                    console.log("left")
                }
                return true;
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                return true;
            },
        });










    }



    componentDidMount() {
        //this.refs.scrollView.scrollTo({ x: (((25) * width / 3)), y: 0, animated: true });
        //this.flatListRef.scrollToIndex({animated: true, index: 26});

        AsyncStorage.getItem('userToken').then((token) => {
            this.scrollToPosition(token)
        })
        //this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width / 3), y: 0 })
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
    }
    scrollToPosition(token) {
        if (this.state.week == 'Week') {
            this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width), y: 0 })
            this.hitGetLeaveListAPI(token, "week", new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].startDate).getTime(), new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].endDate).getTime())

            this.hitGetProjectListAPI(token, "week", new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].startDate).getTime(), new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].endDate).getTime())

        } else if (this.state.week == 'Month') {

            this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width), y: 0 })
            this.hitGetLeaveListAPI(token, "month", this.state.week_Date_Array_Middle_Index + 1)
            this.hitGetProjectListAPI(token, "month", this.state.week_Date_Array_Middle_Index + 1)
        } else {
            this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width), y: 0 })
            this.hitGetLeaveListAPI(token, "year", year_Array[this.state.week_Date_Array_Middle_Index + 2])
            this.hitGetProjectListAPI(token, "year", year_Array[this.state.week_Date_Array_Middle_Index + 2])
        }


    }

    tabs() {
        var arr = []
        for (var i = 2; i < this.state.arrayPassedToTabScrollView.length - 2; i++) {
            var prev = this.state.arrayPassedToTabScrollView[i - 1]
            var current = this.state.arrayPassedToTabScrollView[i]
            var next = this.state.arrayPassedToTabScrollView[i + 1]


            arr.push(this.makeColumn(prev, current, next, i))
        }
        return (
            arr
        )

    }
    makeColumn(prev, item, next, i) {
        return (
            <View
                style={[styles.tabStyle, { width: width, flexDirection: 'row' }]} >
                <TouchableWithoutFeedback onPress={() => prev != '' ? this.onWeekTabPressLeft() : null}>
                    <View style={{ flex: 1, alignItems: "center", justifyContent: 'center' }}>

                        <Text style={[styles.tabText, { color: "#2A56C6", fontWeight: "normal" }]}>
                            {this.state.week == "Week" ?

                                dateConverterUseInWeekTab(new Date(prev.startDate)) + "-" + dateConverterUseInWeekTab(new Date(prev.endDate))
                                :
                                prev
                            }
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <Text style={[styles.tabText, { flex: 1, color: "#000", fontWeight: "bold" }]}>
                    {this.state.week == "Week" ?
                        dateConverterUseInWeekTab(new Date(item.startDate)) + "-" + dateConverterUseInWeekTab(new Date(item.endDate))
                        :
                        item
                    }
                </Text>
                <TouchableWithoutFeedback onPress={() => next != '' ? this.onWeekTabPressRight() : null}>
                    <View style={{ flex: 1, alignItems: "center", justifyContent: 'center' }}>

                        <Text style={[styles.tabText, { color: "#2A56C6", fontWeight: "normal" }]}>
                            {this.state.week == "Week" ?

                                dateConverterUseInWeekTab(new Date(next.startDate)) + "-" + dateConverterUseInWeekTab(new Date(next.endDate))

                                :
                                next
                            }
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        // if (this.props.id) {
        //   this.props.id == 'leaves' ? this.updateSecondsOfTimerWhenScreenChanges() : null
        // }

    }


    keyboardOpen() {
        keyboardOpen = true
        //this.setState({ keyboardOpen: true })
    }

    keyboardClose() {
        keyboardOpen = false
        //this.setState({ keyboardOpen: false })
    }



    hitGetLeaveListAPI(token, action, startDate, endDate) {
        let variables = new FormData();

        variables.append("action", action)
        variables.append("startDate", startDate)
        if (action == 'week') {
            variables.append("endDate", endDate)
        }
        variables.append("userId", this.props.navigation.state.params.data.userId)
        //this.setState({ spinnerVisible: true })
        this.setState({ dataLoading: true })
        //dataLoading
        return webservice(variables, "leave/getlist", 'Post', token)
            .then(response => {
                this.setState({ spinnerVisible: false, dataLoading: false })
                if (response != "error") {
                    if (response.leaveList.length != 0) {

                        var arr = []
                        for (var i = 0; i < response.leaveList.length; i++) {
                            arr.push(
                                {
                                    "validityFrom": response.leaveList[i].startDate,
                                    "validityTo": response.leaveList[i].endDate,
                                    'leaveId': response.leaveList[i].leaveId,
                                    'leaveTypeId': response.leaveList[i].leaveTypeId,
                                    "type": response.leaveList[i].leaveTypeName,
                                    "status": response.leaveList[i].status,
                                    'totalDays': response.leaveList[i].days,
                                    "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas"
                                })
                        }

                        this.setState({ leaveList: arr })
                        if (this.state.TabPress == 'leaves') {
                            this.setState({ dataPassedToFlatList: arr })

                        }
                        //this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)
                    } else {
                        this.setState({ leaveList: [] })
                        if (this.state.TabPress == 'leaves') {
                            this.setState({ dataPassedToFlatList: [] })

                        }
                    }
                    this.totalNumberOfDaysLeave()
                    getLog("Leave List of time sheet page=====++++" + JSON.stringify(response))



                }

            })

    }
    hitGetProjectListAPI(token, action, startDate, endDate) {
        let variables = new FormData();

        variables.append("action", action)
        variables.append("startDate", startDate)
        variables.append("page", 0)
        variables.append("userId", this.props.navigation.state.params.data.userId)
        if (action == 'week') {
            variables.append("endDate", endDate)
        }

        //this.setState({ spinnerVisible: true })
        this.setState({ dataLoading: true })
        //dataLoading
        return webservice(variables, "timesheet/getlist", 'Post', token)
            .then(response => {
                this.setState({ spinnerVisible: false, dataLoading: false })
                if (response != "error") {
                    if (response.timesheetList.length != 0) {

                        var arr = []
                        for (var i = 0; i < response.timesheetList.length; i++) {
                            arr.push(
                                {
                                    "projectName": response.timesheetList[i].projectName,
                                    "projectId": response.timesheetList[i].projectId,
                                    'Activities': response.timesheetList[i].activityCount,
                                    'userId': response.timesheetList[i].userId,
                                    "time": response.timesheetList[i].totalHours,

                                })
                        }
                        // this.totalHoursMinutesOfProjects(response.timesheetList)



                        this.setState({ projectList: arr })
                        if (this.state.TabPress == 'timesheet') {
                            this.setState({ dataPassedToFlatList: arr })

                        }

                    } else {
                        this.setState({ projectList: [] })
                        if (this.state.TabPress == 'timesheet') {
                            this.setState({ dataPassedToFlatList: [] })

                        }
                    }
                    this.totalHoursMinutesOfProjects(response.timesheetList)
                    //this.totalNumberOfDaysLeave()
                    getLog("Timesheet project list List of time sheet page=====++++" + JSON.stringify(response))
                    if (action == 'week') {
                        this.setState({
                            filterDataPassedOnProjectBreakdown: {
                                "startDate": startDate,
                                "action": action,
                                "endDate": endDate,
                                'week_Date_Array_Middle_Index': this.state.week_Date_Array_Middle_Index

                            }
                        })


                    } else {
                        this.setState({
                            filterDataPassedOnProjectBreakdown: {
                                "startDate": startDate,
                                "action": action,
                                "endDate": '',
                                'week_Date_Array_Middle_Index': this.state.week_Date_Array_Middle_Index
                            }
                        })

                    }


                } else {

                }

            })

    }

    onTabPress(tabId) {
        this.setState({ TabPress: tabId })
        if (tabId == 'details') {

        }
        if (tabId == 'timesheet') {
            this.setState({ dataPassedToFlatList: this.state.projectList })
        }
        if (tabId == 'leaves') {
            this.setState({ dataPassedToFlatList: this.state.leaveList })
        }
        //this.setState({ TabPress: !this.state.TabPress })
    }

    navigateToLeaveDescription(item, index) {
        this.props.navigation.navigate('LeaveDescription', { data: item, leaveTabCallBack: this.state.leaveTabCallBack, index: index, leaveTypeListPassToModal: this.state.leaveTypeListPassToModal, leaveTypeList: this.state.leaveTypeList })
    }
    navigateToProjectBreakdown(item) {
        //alert(JSON.stringify(item))
        this.props.navigation.navigate('ProjectBreakdown', { data: item, filterData: this.state.filterDataPassedOnProjectBreakdown, callbackPassedToProjectBreakdown: this.state.callbackPassedToProjectBreakdown, })
    }
    createList(item, index) {
        // var progress = item.progress + "%"
        // var left = (100 - item.progress) + "%"
        var progress = Math.round((item.time / this.state.totalProjectHours) * 100)

        var left = Math.round(100 - progress) + "%"
        progress = progress + '%'
        //getLog(progress+"===="+left)

        return (
            // <TouchableWithoutFeedback onPress={() => this.state.TabPress == 'timesheet' ? this.navigateToProjectBreakdown(item) : this.navigateToLeaveDescription(item, index)}
            <TouchableWithoutFeedback onPress={() => this.state.TabPress == 'timesheet' ? this.navigateToProjectBreakdown(item) : null}
                key={index + "rowListProject"} style={styles.listView}>
                <View style={[styles.listView, {}]}>
                    <View style={[styles.rowView, { paddingLeft: 0, paddingRight: 0 }]}>
                        <View style={styles.projectColumnView}>
                            <Text numberOfLines={1} style={{ fontSize: fontSizes.font14 }}>
                                {this.state.TabPress == 'timesheet' ? item.projectName :
                                    (dateConverterUseInWeekTab(new Date(item.validityFrom)) + "-" + dateConverterUseInWeekTab(new Date(item.validityTo)))}
                            </Text>
                            <Text style={{ fontSize: fontSizes.font14, color: "#BBBBBB", paddingTop: 5 }}>
                                {this.state.TabPress == 'timesheet' ? item.Activities + " Activities" : item.type}
                                {/* {this.state.TabPress == 'timesheet' ? null :
                                    <Text> | </Text>
                                } */}
                                {/* {this.state.TabPress == 'timesheet' ? null :
                                    <Text style={{
                                        fontSize: fontSizes.font14,
                                        color: item.status == 0 ? "red" : item.status == 1 ? "green" : "grey",
                                        paddingTop: 5
                                    }}> {item.status == 0 ? "Pending" : item.status == 1 ? "Approved" : "Rejected"}</Text>
                                } */}
                            </Text>
                        </View>
                        {this.state.TabPress == 'timesheet' ? <View style={[styles.projectColumnView, { flex: 2, marginHorizontal: 5, flexDirection: "row", alignItems: "center" }]}>
                            <View style={{ width: progress, backgroundColor: "#2A56C6", height: 4, }} />
                            <View style={{ width: left, backgroundColor: "#D8D8D8", height: 4 }} />
                        </View> : null}
                        <View style={[styles.projectColumnView, { flex: this.state.TabPress == 'timesheet' ? 1.5 : 0.5, flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>

                            <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                                {this.state.TabPress == 'timesheet' ? hourMinuteConverter(item.time) : item.totalDays > 1 ? item.totalDays + " Days" : item.totalDays + " Day"}
                            </Text>
                            {this.state.TabPress == 'timesheet' ?
                                <Image source={require('../images/rightArrow.png')} />
                                : null}

                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    makeTabs() {
        return (
            <View style={styles.tabView}>
                {/* detail tab */}
                <TouchableOpacity onPress={() => this.onTabPress('details')}
                    style={[styles.tabStyle, { flex: 1, borderBottomColor: "white", borderBottomWidth: this.state.TabPress == 'details' ? 2 : 0 }]} >
                    <Text style={[styles.tabText, { fontSize: fontSizes.font14, fontWeight: 'bold' }]}>
                        Details
                </Text>
                </TouchableOpacity>
                {/* first tab */}
                <TouchableOpacity onPress={() => this.onTabPress('timesheet')}
                    style={[styles.tabStyle, { flex: 1, borderBottomColor: "white", borderBottomWidth: this.state.TabPress == 'timesheet' ? 2 : 0 }]} >
                    <Text style={[styles.tabText, { fontSize: fontSizes.font14, fontWeight: 'bold' }]}>
                        Timesheet
                </Text>
                </TouchableOpacity>
                {/* second tab */}
                <TouchableOpacity onPress={() => this.onTabPress('leaves')}
                    style={[styles.tabStyle, { flex: 1, borderBottomColor: "white", borderBottomWidth: this.state.TabPress == 'leaves' ? 2 : 0 }]}   >
                    <Text style={[styles.tabText, { fontSize: fontSizes.font14, fontWeight: 'bold' }]}>
                        Leaves
                              </Text>
                </TouchableOpacity>
                {/* third tab */}

            </View>
        )
    }
    // onWeekTabPressLeft() {
    //     if (this.state.week == "Week") {
    //         if (this.state.week_Date_Array_Middle_Index > 1) {

    //             this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
    //             setTimeout(() => {
    //                 //alert(this.state.week_Date_Array_Middle_Index)
    //                 this.makeWeekDateTabArray(this.state.userToken)
    //             }, 300)
    //         }
    //     } else if (this.state.week == "Month") {
    //         if (this.state.week_Date_Array_Middle_Index > 1) {
    //             console.log("month=======" + this.state.week_Date_Array_Middle_Index)
    //             this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) - 1)
    //             this.hitGetProjectListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) - 1)

    //             this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
    //             setTimeout(() => {
    //                 this.setState({ weekArray: [monthArr[this.state.week_Date_Array_Middle_Index - 1], monthArr[this.state.week_Date_Array_Middle_Index], monthArr[this.state.week_Date_Array_Middle_Index + 1]] })
    //             }, 300)
    //         }
    //     } else {
    //         this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })

    //         setTimeout(() => {
    //             this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
    //             this.hitGetProjectListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
    //             this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
    //         }, 300)
    //     }



    // }
    // onWeekTabPressRight() {
    //     if (this.state.week == "Week") {
    //         if (this.state.week_Date_Array_Middle_Index < 51) {
    //             this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
    //             setTimeout(() => {
    //                 this.makeWeekDateTabArray(this.state.userToken)
    //             }, 300)
    //         }
    //     } else if (this.state.week == "Month") {
    //         if (this.state.week_Date_Array_Middle_Index < 10) {
    //             console.log("month=======" + this.state.week_Date_Array_Middle_Index)
    //             this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) + 1)

    //             this.hitGetProjectListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) + 1)
    //             //alert(monthArr[this.state.week_Date_Array_Middle_Index + 1])
    //             //alert(monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
    //             this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
    //             setTimeout(() => {
    //                 this.setState({ weekArray: [monthArr[this.state.week_Date_Array_Middle_Index - 1], monthArr[this.state.week_Date_Array_Middle_Index], monthArr[this.state.week_Date_Array_Middle_Index + 1]] })
    //             }, 300)
    //         }
    //     } else {
    //         this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
    //         setTimeout(() => {
    //             this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)

    //             this.hitGetProjectListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)


    //             this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
    //         }, 300)
    //     }
    // }
    onWeekTabPressLeft() {
        this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
        setTimeout(() => {
            this.scrollToPosition(this.state.userToken)
        }, 100)

    }
    onWeekTabPressRight() {
        this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
        setTimeout(() => {
            this.scrollToPosition(this.state.userToken)
        }, 100)
    }
    makeWeekDateTabArray(token) {
        var index = this.state.week_Date_Array_Middle_Index
        var weekArray = []
        weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].endDate)))
        weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index].endDate)))
        weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].endDate)))
        this.setState({ weekArray: weekArray })

        //alert(new Date(week_Date_Array[index].startDate) + new Date(week_Date_Array[index].endDate))
        this.hitGetLeaveListAPI(token, "week", new Date(week_Date_Array[index].startDate).getTime(), new Date(week_Date_Array[index].endDate).getTime())

        this.hitGetProjectListAPI(token, "week", new Date(week_Date_Array[index].startDate).getTime(), new Date(week_Date_Array[index].endDate).getTime())


        // this.sortLeaveListByWeek(index)
    }
    // sortLeaveListByWeek(index) {
    //   var arr = []
    //   for (var i = 0; i < this.state.leaveList.length; i++) {
    //     if ((this.state.leaveList[i].validityTo >= week_Date_Array[index].startDate) && (this.state.leaveList[i].validityFrom <= week_Date_Array[index].endDate)) {
    //       arr.push(this.state.leaveList[i])
    //     }
    //   }
    //  // alert(JSON.stringify(this.state.leaveList[0]))
    //   getLog("==="+JSON.stringify(arr))
    //   this.setState({ leaveList: arr })
    // }
    pickerDropdownModalRowSelected(item) {
        if (this.state.PickerDropdownModalType == "Select Project") {
            this.setState({ selectProject: item.projectName, selectedProjectId: item.projectId })

            //this code clear the interval also reset the global variables
            //cancel the push notification and reset all the states
            clearInterval(interval);
            global.flag = false;
            global.selectedProject = '';
            global.play = false
            backgroundSecond = 0
            PushNotification.cancelLocalNotifications({ id: '123' });
            this.setState({
                second: 0, playPauseText: 'Play', minute: 0,
                playPauseImage: require('../images/timesheetIcon/playIcon.png'),
                stopImage: require('../images/timesheetIcon/greyStopIcon.png'),

                play: false
            })

        } else if (this.state.PickerDropdownModalType == 'Select Leave Type') {
            this.setState({ leaveTypeOnLeaveModal: item.type })
        }
        else {
            this.setState({ week: item })

            // if (item == "Week") {
            //   this.setState({ week_Date_Array_Middle_Index: 26 })
            //   setTimeout(() => {
            //     this.makeWeekDateTabArray(this.state.userToken)
            //   }, 300)

            // } else if (item == "Month") {

            //   var month = new Date().getMonth()
            //   this.setState({ week_Date_Array_Middle_Index: month })
            //   if (month > 0 && month < 11) {
            //     setTimeout(() => {
            //       this.setState({ weekArray: [monthArr[month - 1], monthArr[month], monthArr[month + 1]] })
            //     }, 300)
            //   } else {
            //     if (month == 0) {
            //       setTimeout(() => {
            //         this.setState({ weekArray: [monthArr[month], monthArr[month + 1], monthArr[month + 2]], week_Date_Array_Middle_Index: 1 })
            //       }, 300)
            //     } else {
            //       setTimeout(() => {
            //         this.setState({ weekArray: [monthArr[month - 2], monthArr[month - 1], monthArr[month]], week_Date_Array_Middle_Index: 10 })
            //       }, 300)
            //     }

            //   }
            //   setTimeout(() => {
            //     this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
            //     this.hitGetProjectListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
            //   }, 300)


            // } else {
            //   var year = new Date().getFullYear()
            //   this.setState({ week_Date_Array_Middle_Index: year })
            //   setTimeout(() => {
            //     this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
            //     this.hitGetProjectListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
            //     this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
            //   }, 300)
            // }
            if (item == "Week") {
                this.setState({ week_Date_Array_Middle_Index: 24, arrayPassedToTabScrollView: week_Date_Array, week: "Week" })
                setTimeout(() => {
                    this.scrollToPosition(this.state.userToken)
                }, 300)
            } else if (item == "Month") {
                this.scroller.scrollTo({ x: 0, y: 0, animated: false })
                var month = new Date().getMonth()
                this.setState({ arrayPassedToTabScrollView: monthArr, week: "Month", week_Date_Array_Middle_Index: month })
                setTimeout(() => {
                    this.scrollToPosition(this.state.userToken)

                }, 300)
            } else {
                this.scroller.scrollTo({ x: 0, y: 0, animated: false })
                var year = new Date().getFullYear()
                this.setState({ week_Date_Array_Middle_Index: year_Array.indexOf(year - 2), arrayPassedToTabScrollView: year_Array, week: "Year" })
                setTimeout(() => {
                    this.scrollToPosition(this.state.userToken)
                }, 300)
            }


        }
        this.setState({ pickerDropdownModalVisible: false })
    }

    // onFloatingButtonPressed() {
    //     this.state.TabPress=='leaves' ?
    //         this.setState({
    //             modalVisible: true, leaveModalErrorText: '',
    //             startDate: '',
    //             endDate: '',
    //             leaveTypeOnLeaveModal: 'Leave Type',
    //             totalDays: "" + 0,
    //             reasonTextInputData: '',
    //             startDateToShowOnMOdal: '',
    //             endDateToShowOnModal: '',
    //             leaveModalErrorText: ''
    //         })
    //         : this.props.navigation.navigate("CreateTimesheet", {
    //             data: {
    //                 returnCallBack: this.state.returnCallBack,
    //                 filterData: this.state.filterDataPassedOnProjectBreakdown
    //                 //projectName: this.state.selectProject,
    //             }
    //         })
    // }
    callBackCalledOnActivityAdd = (token, data) => {
        //alert(data)
        // this.state.projectList.push({ "projectName": data.projectName, 'Activities': data.activity, 'progress': 0, 'time': data.time })
        // this.setState({ projectList: this.state.projectList })
        this.hitGetProjectListAPI(token, data.action, data.startDate, data.endDate)
        //this.hitGetAllProjectListApi(token)

        //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
    }
    callbackCalledOnChangesOnProjectBreakdown = (token, data) => {
        this.hitGetProjectListAPI(token, data.action, data.startDate, data.endDate)
        //this.hitGetAllProjectListApi(token)
    }
    callBackCalledOnEditDeleteLeave = (data) => {
        if (data.type == "delete") {
            var arr = []
            for (var i = 0; i < this.state.leaveList.length; i++) {
                if (i != data.index) {
                    arr.push(this.state.leaveList[i])
                }
            }
            this.setState({ leaveList: arr })
        } else {

            var arr = this.state.leaveList
            arr[data.index].validityFrom = data.editedData.validityFrom
            arr[data.index].validityTo = data.editedData.validityTo
            arr[data.index].status = data.editedData.status
            arr[data.index].totalDays = data.editedData.totalDays
            arr[data.index].reason = data.editedData.reason
            arr[data.index].type = data.editedData.type

            this.setState({ leaveList: arr })

        }
        this.totalNumberOfDaysLeave()


        //this callback is called when we click on delete button of leave description scren and edit leave description on leave description screen
    }
    getDateFromModal(date) {
        getLog("getDateFromModal" + date)
        if (this.state.datePickerType == 'start') {
            this.setState({ startDate: date, datePickerType: '', minDate: date, datePickerVisible: false, endDateToShowOnModal: "", endDate: '' })
            var d = dateConverterUseInHeader(date)
            setTimeout(() => {
                this.setState({ startDateToShowOnMOdal: d })
            }, 200)
        } if (this.state.datePickerType == 'end') {
            this.setState({ endDate: date, datePickerType: '', datePickerVisible: false, })
            var d = dateConverterUseInHeader(date)
            setTimeout(() => {
                this.setState({ endDateToShowOnModal: d })
                this.totalDaysCalculate()
            }, 200)

        }


    }
    totalDaysCalculate() {
        getLog(this.state.endDate)
        var date1 = new Date(this.state.startDate);
        var date2 = new Date(this.state.endDate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
        this.daysTextInputChangeOnLeaveModal("" + diffDays)
        // this.setState({ totalDays: diffDays + 1 })
        // setTimeout(() => {
        //   console.log(this.state.totalDays)
        // }, 300)



    }
    startDateVisibleFunction() {
        this.setState({ datePickerVisible: true, datePickerType: 'start', minDate: '' })
    }
    endDateVisibleFunction() {
        if (this.state.startDate == '') {
            alert("Please select start date first")
        } else {
            this.setState({ datePickerVisible: true, datePickerType: 'end', minDate: this.state.minDate })
        }

    }
    totalNumberOfDaysLeave() {
        setTimeout(() => {
            var days = 0
            for (var i = 0; i < this.state.leaveList.length; i++) {
                getLog(this.state.leaveList.length + "====" + this.state.leaveList[i].totalDays)
                days = days + parseFloat(this.state.leaveList[i].totalDays)

            }

            this.setState({ totalLeaveDays: days })
        }, 300)
    }
    totalHoursMinutesOfProjects(response) {

        var minutes = 0
        this.setState({ totalProjectHours: 0 })
        for (var i = 0; i < response.length; i++) {
            //getLog(this.state.leaveList.length + "====" + this.state.leaveList[i].totalDays)
            minutes = minutes + Number(response[i].totalHours)

        }


        this.setState({ totalProjectHours: minutes })

    }

    hitAddLeaveAPI() {
        var arr = this.state.leaveList
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("leaveTypeId", this.state.leaveTypeId)
        variables.append("days", this.state.totalDays)
        variables.append("startDate", new Date(this.state.startDate).getTime())
        variables.append("endDate", new Date(this.state.endDate).getTime())
        variables.append("reason", this.state.reasonTextInputData)

        this.setState({ spinnerVisible: true })
        return webservice(variables, "leave/add", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    // var arr = this.state.leaveList
                    arr.push(
                        {
                            "validityFrom": this.state.startDate,
                            "validityTo": this.state.endDate,
                            'type': this.state.leaveTypeOnLeaveModal,
                            'status': 0,
                            'totalDays': this.state.totalDays,
                            "reason": this.state.reasonTextInputData,
                            "leaveTypeId": this.state.leaveTypeId
                        }
                    )
                    this.setState({ leaveList: arr })
                    //this.totalNumberOfDaysLeave()
                    setTimeout(() => {
                        this.setState({ modalVisible: false })
                        this.setState({
                            startDate: '',
                            endDate: '',
                            leaveTypeOnLeaveModal: 'Leave Type',
                            //totalDays: "" + 0,
                            reasonTextInputData: '',
                            startDateToShowOnMOdal: '',
                            endDateToShowOnModal: '',
                            leaveModalErrorText: ''


                        })
                    }, 300)
                } else {
                    getLog("Leave  add response=====++++" + JSON.stringify(response))

                }

            })
    }

    saveButtonClicked() {

        if (this.state.leaveTypeOnLeaveModal == "Leave Type") {
            this.setState({ leaveModalErrorText: "*Please select leave type." })
            return
        }
        if (this.state.startDate == "") {
            this.setState({ leaveModalErrorText: "*Please select start date." })
            return
        }
        if (this.state.endDate == "") {
            this.setState({ leaveModalErrorText: "*Please select end date." })
            return
        }
        this.hitAddLeaveAPI()
        // var arr = this.state.leaveList
        // arr.push(
        //   {
        //     "validityFrom": this.state.startDate,
        //     "validityTo": this.state.endDate,
        //     'type': this.state.leaveTypeOnLeaveModal,
        //     'status': "Pending",
        //     'totalDays': this.state.totalDays,
        //     "reason": this.state.reasonTextInputData
        //   }
        // )
        // this.setState({ leaveList: arr })
        // this.totalNumberOfDaysLeave()
        // setTimeout(() => {
        //   this.setState({ modalVisible: false })
        //   this.setState({
        //     startDate: '',
        //     endDate: '',
        //     leaveTypeOnLeaveModal: 'Leave Type',
        //     totalDays: "" + 0,
        //     reasonTextInputData: '',
        //     startDateToShowOnMOdal: '',
        //     endDateToShowOnModal: '',
        //     leaveModalErrorText: ''


        //   })
        // }, 300)

        //alert(this.state.reasonTextInputData)
    }
    modalClose() {
        if (keyboardOpen) {
            Keyboard.dismiss()
        } else {
            this.setState({ modalVisible: false })
        }

    }


    onProjectSelectDropdown() {
        this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Select Project", pickerDropdownModalListData: this.state.projectListPassedToSelectProjectDropdown })
    }

    onWeekSelectDropdown() {
        this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Week", pickerDropdownModalListData: ["Week", "Month", "Year"] })
    }
    onLeaveModalselectLeaveTypeClicked(index, value) {

        this.setState({ leaveTypeOnLeaveModal: value, leaveTypeId: this.state.leaveTypeList[index].leaveTypeId })
    }
    daysTextInputChangeOnLeaveModal(text) {
        this.setState({ totalDays: text })
    }


    pickerDropdownSelectClickedOfSelectProject(item) {
        if (item != '') {
            this.setState({ selectProject: item.projectName, selectedProjectId: item.projectId })

            //this code clear the interval also reset the global variables
            //cancel the push notification and reset all the states
            clearInterval(interval);
            global.flag = false;
            global.selectedProject = '';
            global.play = false
            backgroundSecond = 0
            PushNotification.cancelLocalNotifications({ id: '123' });
            this.setState({
                second: 0, playPauseText: 'Play', minute: 0,
                playPauseImage: require('../images/timesheetIcon/playIcon.png'),
                stopImage: require('../images/timesheetIcon/greyStopIcon.png'),
                selectProjectModal: false,
                play: false, selectedProjectItem: ''
            })
            var arr = this.state.projectListPassedToSelectProjectDropdown
            for (var i = 0; i < arr.length; i++) {
                arr[i].select = false

            }
        } else {
            alert("Please select project.")
        }


    }
    pickerDropdownCloseClickedOfSelectProject() {

        var arr = this.state.projectListPassedToSelectProjectDropdown
        // for (var i = 0; i < arr.length; i++) {
        //   arr[i].select = false

        // }
        this.setState({ selectProjectModal: false, selectedProjectItem: '', projectListPassedToSelectProjectDropdown: this.state.projectListContainAllProjectOfSelectProjectDropdown })
    }
    onChangeOfSearchTextInputOfSelectProjectModal(text) {
        if (text != '') {
            var arr = []
            var typeArr = this.state.projectListContainAllProjectOfSelectProjectDropdown
            for (var i = 0; i < typeArr.length; i++) {

                if (typeArr[i].projectName.toLowerCase().indexOf(text.toLowerCase()) != -1) {
                    arr.push(typeArr[i])
                }
            }
            this.setState({ projectListPassedToSelectProjectDropdown: arr })
        } else {
            this.setState({ projectListPassedToSelectProjectDropdown: this.state.projectListContainAllProjectOfSelectProjectDropdown })
        }



    }
    //
    onChange(text, type) {
        if (type === 'Name') {

            this.setState({ name: text })
        }
        if (type === 'Email') {
            this.setState({ email: text })
        }
    }
    nameEdit() {
        if (this.state.nameEdit) {
            if (this.state.name != '') {
                this.hitEditEmployeeDetailAPI()
            } else {
                alert('Please enter name.')
            }


        } else {
            if (this.state.emailEdit) {
                alert("Please save email id first.")
            } else {
                this.setState({ nameEdit: true })
            }

        }

    }
    emailEdit() {
        var emailRegex = (/^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i)

        if (this.state.emailEdit) {
            if (this.state.email == '') {
                alert('Please enter email.')


            } else if (!emailRegex.test(this.state.email)) {
                alert('Please enter valid email.')

            } else {
                this.hitEditEmployeeDetailAPI()
            }

        } else {
            if (this.state.nameEdit) {
                alert("Please save fullname first.")
            } else {
                this.setState({ emailEdit: true })
            }

        }
    }
    hitEditEmployeeDetailAPI(type) {
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("employeeName", this.state.name)
        variables.append("employeeEmail", this.state.email)
        variables.append("userId", this.state.userId)
        variables.append("isActive", !this.state.activeStatus ? '1' : '0')


        this.setState({ spinnerVisible: true })
        return webservice(variables, "employee/edit", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })

                if (response != "error") {
                    this.setState({ emailEdit: false })
                    this.setState({ nameEdit: false })
                    if (type == 'switch') {
                        this.setState({ activeStatus: !this.state.activeStatus })
                    }

                    setTimeout(() => {
                        this.props.navigation.state.params.employeeDetailCallBack.callBack(this.state.searchtextPassedbackToManagePage)
                        //this.setState({ addEmployeeModalVisible: false })
                    }, 300)
                } else {
                    //this.setState({ activeStatus: !this.state.activeStatus })

                }
                getLog("response of edit user detail")

            })
    }
    onActiveSwitchPress() {



        this.hitEditEmployeeDetailAPI('switch')


    }
    makeUserDetailView() {
        return (
            <View style={{ width: width - 20 }}>
                <View style={styles.detailRowView}>
                    <View style={{ flex: 1, justifyContent: "center", marginLeft: 15 }}>
                        <Text style={styles.textInputText}>Name</Text>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={[styles.textInput, { color: this.state.nameEdit ? '#000' : '#979797' }]}
                            placeholder="Name"
                            placeholderTextColor="#979797"
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => this.onChange(text, 'Name')}
                            value={this.state.name}
                            editable={this.state.nameEdit ? true : false}
                            autoFocus={this.state.nameEdit ? true : false}

                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                    </View>
                    <View style={styles.editView}>
                        <TouchableWithoutFeedback onPress={() => this.nameEdit()}>
                            <Image source={this.state.nameEdit ? require('../images/appSettingIcons/checkIconGrey.png') : require('../images/appSettingIcons/editIconGrey.png')} resizeMode="cover"
                            />
                        </TouchableWithoutFeedback>
                    </View>
                </View>

                <View style={styles.detailRowView}>
                    <View style={{ flex: 1, justifyContent: "center", marginLeft: 15 }}>
                        <Text style={styles.textInputText}>Email</Text>
                    </View>
                    <View style={styles.textInputView}>
                        <TextInput
                            style={[styles.textInput, { color: this.state.emailEdit ? '#000' : '#979797' }]}
                            placeholder="Email"
                            placeholderTextColor="#979797"
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => this.onChange(text, 'Email')}
                            value={this.state.email}
                            editable={this.state.emailEdit ? true : false}
                            autoFocus={this.state.emailEdit ? true : false}

                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                    </View>
                    <View style={styles.editView}>
                        <TouchableWithoutFeedback onPress={() => this.emailEdit()}>
                            <Image source={this.state.emailEdit ? require('../images/appSettingIcons/checkIconGrey.png') : require('../images/appSettingIcons/editIconGrey.png')} resizeMode="cover"
                            />
                        </TouchableWithoutFeedback>
                    </View>
                </View>
                <TouchableWithoutFeedback  >
                    <View style={[styles.detailRowView, { justifyContent: 'space-between', paddingLeft: 15 }]}>
                        <View style={{ justifyContent: "center", }}>
                            <Text style={[styles.textInputText, { color: this.state.activeStatus ? "#000" : "red" }]}>{this.state.activeStatus ? 'Active' : "Disabled"}</Text>
                        </View>



                        <TouchableWithoutFeedback onPress={() => this.onActiveSwitchPress()}>
                            <View style={styles.rightArrowView}>
                                <Image source={this.state.activeStatus ? require('../images/switchGreen.png') : require('../images/switchRed.png')} resizeMode="cover"
                                />
                            </View>
                        </TouchableWithoutFeedback>


                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
    render() {
        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Spinner visible={this.state.spinnerVisible} />
                    <Header
                        HeaderLeftText={"Employee Name"}
                        headerType='back'
                        HeaderRightText={this.state.week}

                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIconOnPress={() => this.onWeekSelectDropdown()}
                    />
                    {/* { this.state.TabPress=='details' ? null :
                        <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />
                    } */}



                    {/* <LeaveModal
                        modalVisible={this.state.modalVisible}
                        datePickerVisible={this.state.datePickerVisible}
                        datePickerVisibleFunction={() => this.setState({ datePickerVisible: true })}
                        startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                        endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                        dateSelected={(date) => this.getDateFromModal(date)}
                        startDate={this.state.startDateToShowOnMOdal}
                        endDate={this.state.endDateToShowOnModal}
                        daysTextInputData={this.state.totalDays}
                        daysTextInputChange={(text) => this.daysTextInputChangeOnLeaveModal(text)}
                        datePickerCloseFunction={() => this.setState({ datePickerVisible: false })}
                        minDate={this.state.minDate == '' ? new Date('01/01/1990') : this.state.minDate}
                        saveButtonClicked={() => this.saveButtonClicked()}
                        reasonTextInputDataChange={(text) => this.setState({ reasonTextInputData: text })}
                        reasonTextInputData={this.state.reasonTextInputData}
                        modalClose={() => this.modalClose()}
                        TitleText="Add Leave" TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
                        leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassToModal}
                        selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
                        errorText={this.state.leaveModalErrorText}
                    /> */}
                    {/* <SelectProjectModal
                        pickerDropdownModalVisible={this.state.selectProjectModal}
                        pickerDropdownModalClose={() => this.pickerDropdownCloseClickedOfSelectProject()}
                        titleText="Select Project"
                        type='singleSelect'
                        pickerDropdownModalListData={this.state.projectListPassedToSelectProjectDropdown}
                        extraData={this.state}
                        onChangeOfSearchTextInput={(text) => this.onChangeOfSearchTextInputOfSelectProjectModal(text)}
                        //pickerDropdownSelectClicked={() => this.pickerDropdownSelectClickedOfSelectProject(this.state.selectedProjectItem)}
                        pickerDropdownModalRow={({ item, index }) =>
                            <TouchableWithoutFeedback key={"abc" + index}
                                onPress={() => this.customProjectListForModalRowSelected(item, index)}>
                                <View style={{ padding: 12, paddingLeft: 25, alignItems: "center", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1, flexDirection: "row" }}>
                                    
                                    <Text style={{ fontSize: fontSizes.font16, color: "#333333", }}>
                                        {item.projectName}
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        }
                    /> */}
                    <PickerDropdownModal
                        pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
                        pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
                        titleText="Select Type"
                        //titleText={this.state.PickerDropdownModalType == "Week" ? this.state.week : this.state.PickerDropdownModalType}
                        pickerDropdownModalListData={this.state.pickerDropdownModalListData}
                        pickerDropdownModalRow={({ item, index }) =>
                            <TouchableWithoutFeedback key={"pickerDropdown" + index} onPress={() => this.pickerDropdownModalRowSelected(item)}>
                                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                                    <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                                        {item.projectName ? item.projectName : item.type ? item.type : item}
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        }
                    />




                    <View style={styles.mainContainer}>
                        {this.makeTabs()}
                        {/* <TimeLeaveTab onTabPress={() => this.onTabPress()} tabPress={this.state.TabPress} /> */}
                        {/* {this.state.weekArray ? this.weekTabs() : null} */}
                        <View style={{ height: this.state.TabPress == 'details' ? 0 : 35 }}>
                            <ScrollView
                                pagingEnabled

                                style={{ height: 35, backgroundColor: "#DADADA", width: width }}
                                showsHorizontalScrollIndicator={false}
                                onChangeVisibleColumns={(visibleRows) => alert(visibleRows)}
                                //onScroll={(event)=>alert()}
                                //scrollEventThrottle={16}
                                //onScrollEndDrag={() => this.scrollToPosition()}
                                ref={(scroller) => { this.scroller = scroller }}
                                horizontal
                                {...this.wrapperPanResponder.panHandlers}
                            >
                                {this.state.week_Date_Array_Middle_Index > -3 ? this.tabs() : null}
                            </ScrollView>
                        </View>

                        <ScrollView
                            key="scrollview" keyboardShouldPersistTaps="always" >
                            <View style={[styles.projectListContainer, { paddingBottom: 50 }]}>
                                {this.state.TabPress == 'details' ? null :
                                    <View>
                                        {this.state.TabPress == 'leaves' && this.state.leaveList.length == 0 ? null :
                                            <View style={styles.headerView}>
                                                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                                    <Text style={styles.ProjectHeaderText}>
                                                        {this.state.TabPress == 'timesheet' ? "Projects" : "On"}
                                                    </Text>
                                                    <Text style={styles.ProjectHeaderTime}>

                                                        {this.state.TabPress == 'timesheet' ? hourMinuteConverter(this.state.totalProjectHours) : this.state.totalLeaveDays > 1 ? this.state.totalLeaveDays + " Days" : this.state.totalLeaveDays + " Day"}

                                                    </Text>
                                                </View>
                                            </View>
                                        }
                                    </View>
                                }
                                {this.state.TabPress == 'details' ?
                                    this.makeUserDetailView()
                                    :
                                    this.state.dataLoading ?
                                        <Text style={styles.ProjectHeaderText}>
                                            Loading
                                    </Text>
                                        :
                                        <FlatList
                                            data={this.state.dataPassedToFlatList}
                                            renderItem={({ item, index }) => this.createList(item, index)}
                                            keyExtractor={key => key.index}
                                            extraData={this.state}
                                        />
                                }
                                {!this.state.dataLoading ?
                                    this.state.TabPress == 'timesheet' ? this.state.projectList.length == 0 ?
                                        <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font14, marginTop: 15, color: "#898989" }]}>
                                            No project activity found for this time period
                    </Text>
                                        : null
                                        : (this.state.TabPress == 'leaves' && this.state.leaveList.length == 0) ?
                                            <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font14, marginTop: 15, color: "#898989" }]}>
                                                No leaves found for this time period
                  </Text> : null
                                    : null
                                }
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#fff', justifyContent: "flex-start", },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    tabText: { fontSize: fontSizes.font12, color: "white", textAlign: 'center', },
    instructions: { textAlign: 'center', color: '#333333', marginBottom: 5, },
    mainContainer: { flex: 1, justifyContent: "center", alignItems: 'center', },
    tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
    weekTabView: { height: 35, width: width, flexDirection: 'row', backgroundColor: "#DADADA" },
    tabStyle: { alignItems: "center", justifyContent: "center" },
    projectListContainer: { margin: 10, width: width - 20, },
    ProjectHeaderText: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'center' },
    ProjectHeaderTime: { fontSize: fontSizes.font14, color: "#FF9800", textAlign: 'center' },
    headerView: { borderBottomWidth: .5, padding: 10, borderBottomColor: '#979797' },
    listView: { borderBottomWidth: .5, padding: 0, paddingTop: 5, paddingBottom: 5, borderBottomColor: '#BBBBBB' },
    rowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
    projectColumnView: { flex: 1.5, justifyContent: "center", },
    //TimerContainer: { height: 100, width: width - 20, borderRadius: 6, borderWidth: .5, borderColor: "white", shadowOffset: { width: 0, height: 3, }, shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 5 }
    TimerContainer: { width: width - 20, borderRadius: 6, borderWidth: 0, paddingRight: 10, paddingBottom: 10, borderColor: "#E5E5E588", borderRadius: 5, padding: 5 },
    detailContainer: { width: width, marginTop: 10 },
    detailRowView: { height: 50, width: width - 20, flexDirection: "row", borderBottomColor: '#E1E2E4', borderBottomWidth: 1 },
    textInputText: { fontSize: fontSizes.font14, color: "#000" },
    textInputView: { flex: 3, marginHorizontal: 10 },
    textInput: { height: 50, textAlign: 'right' },
    editView: { flex: .5, marginRight: 15, alignItems: "flex-end", justifyContent: "center" },
    rightArrowView: { marginRight: 15, alignItems: "flex-end", justifyContent: "center" }

});
